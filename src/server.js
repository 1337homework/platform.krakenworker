require('./utils/env')
const rabbitmq = require('./interfaces/rabbit')
const websockets = require('./interfaces/websockets')
const { krakenWorkerServiceHandler } = require('./domain/services/krakenWorkerServiceHandler')
const { logger } = require('./utils/logger')
rabbitmq
  .openConnection()
  .then(() => {
    rabbitmq.setConsumer(krakenWorkerServiceHandler)
    websockets.runSocketServer()
    logger.log({ level: 'info', message: 'Kraken Worker Connected to RabbitMQ. Listening for commands...' })
  })
  .catch((err) => {
    logger.log({ level: 'error', message: `${err.message} - ${err.stack}` })
    process.exit(1)
  })
