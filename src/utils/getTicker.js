const { logger } = require('../utils/logger')
const generalConfig = require('../configuration/general')
const request = !generalConfig.proxyConnStr
  ? require('request-promise')
  : require('request-promise').defaults({
    proxy: generalConfig.proxyConnStr,
    strictSSL: false
  })

const getTicker = () => {
  return request({
    uri: generalConfig.krakenAPIUrl,
    method: 'GET',
    qs: {
      pair: 'XRPUSD,ETHUSD,XBTUSD'
    }
  }).then((result) => {
    let data = JSON.parse(result)
    data.marketName = 'kraken'
    data.eventTime = Date.now()
    console.log(data.eventTime)
    return data
  }).catch((error) => {
    logger.log({
      level: 'error',
      message: `Error polling Kraken API, ${error}`
    })
    return new Error(error)
  })
}

module.exports = getTicker
