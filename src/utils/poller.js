const EventEmitter = require('events')

class Poller extends EventEmitter {
  constructor(timeout = 5000) {
    super()

    if (this.interval) {
      return
    }

    this.timeout = timeout
    this.interval = null
  }

  stop() {
    clearTimeout(this.interval)
    this.interval = null
  }

  poll() {
    this.interval = setTimeout(() => this.emit('poll'), this.timeout)
  }

  onPoll(cb) {
    this.on('poll', cb)
  }
}

module.exports = Poller
