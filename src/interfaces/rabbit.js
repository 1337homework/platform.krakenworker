const amqp = require('amqplib')
const { rabbit } = require('../configuration')
const {
  TOPIC_MARKET_ORDER,
  QUEUE_KRAKEN_HANDLER,
  ROUTING_KEY_UPDATE
} = require('../configuration/constants').communicationTopology
const { logger } = require('../utils/logger')

let rabbitChannel = null

const assertBinanceWorkerChannelState = () => {
  return rabbitChannel.assertExchange(TOPIC_MARKET_ORDER, 'topic', { durable: true }).then(() => {
    return rabbitChannel.assertQueue(QUEUE_KRAKEN_HANDLER, { durable: true }).then((result) => {
      return rabbitChannel.bindQueue(result.queue, TOPIC_MARKET_ORDER, 'kraken.*')
    })
  })
}

const openConnection = () => {
  return amqp.connect(rabbit.connStr)
    .then((conn) => {
      return conn.createChannel()
    })
    .then((channel) => {
      rabbitChannel = channel
      return assertBinanceWorkerChannelState()
    })
    .catch((err) => {
      logger.log({ level: 'error', message: `Failed connect to RabbitMQ, reconnecting... ${JSON.stringify(err)}` })
      return new Promise((resolve) => {
        setTimeout(() => resolve(openConnection()), rabbit.retryInterval)
      })
    })
}

const setConsumer = (consumer) => {
  return rabbitChannel.consume(QUEUE_KRAKEN_HANDLER, consumer)
}

const publishStatusUpdate = (message) => {
  return rabbitChannel.publish(
    TOPIC_MARKET_ORDER,
    ROUTING_KEY_UPDATE,
    Buffer.from(JSON.stringify(message)),
    {
      persistent: true,
      contentType: 'application/json'
    }
  )
}

const ack = (message, allUpTo = false) => {
  if (!message) {
    return
  }
  rabbitChannel.ack(message, allUpTo)
}

module.exports = {
  openConnection,
  setConsumer,
  publishStatusUpdate,
  ack
}
