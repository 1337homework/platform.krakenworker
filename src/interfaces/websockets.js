const WebSocket = require('ws')
const generalConfig = require('../configuration/general')
const { logger } = require('../utils/logger')
const uuid = require('uuid/v4')

let wsServer = null
let interval = null

const runSocketServer = () => {
  wsServer = new WebSocket.Server({ hsot: generalConfig.wssHost, port: generalConfig.wssPort })

  logger.log({ level: 'info', message: `WebSocket server is waiting for connections on port ${generalConfig.wssPort}` })

  wsServer.on('connection', (ws) => {
    ws.isAlive = true
    ws.clientId = uuid()
    ws.on('pong', function pong() { this.isAlive = true })
    ws.send(JSON.stringify({
      status: `Client with id ${ws.clientId} connected`
    }))
  })

  wsServer.on('close', (msg) => {
    logger.log({ level: 'info', message: `Websockets server shutting down. ${msg}` })
  })

  wsServer.broadcast = (data) => {
    wsServer.clients.forEach((ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        ws.send(JSON.stringify(data))
      }
    })
  }

  interval = setInterval(() => {
    wsServer.clients.forEach((ws) => {
      if (ws.isAlive === false) {
        logger.log({ level: 'info', message: 'Disconnecting inactive client' })
        return ws.terminate()
      }

      ws.isAlive = false
      ws.ping('', false, true)
    })
  }, 30000)
}

const closeSocketServer = () => {
  clearInterval(interval)
  interval = null
  wsServer.close()
}

const getServer = () => {
  return wsServer
}

module.exports = {
  getServer,
  closeSocketServer,
  runSocketServer
}
