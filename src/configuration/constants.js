module.exports = {
  reasonCodes: {
    NOT_SUCCESSFULL_STATUS_CODE_FROM_KRAKEN: 'KrakenNotAccepted',
    EXCEPTION_TRYING_CONNECTING_TO_KRAKEN: 'KrakenNotReachable',
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DISCONNECTED_FROM_SERVICE: 'DisconnectedFromService',
    STREAM_COMPLETED: 'KrakenPollingStopped',
    STREAM_STARTED: 'KrakenPollingStarted'
  },
  communicationTopology: {
    TOPIC_MARKET_ORDER: 'SagaTaskOrder',
    ROUTING_KEY_UPDATE: 'update',
    ROUTING_KEY_STOP_KRAKEN: 'kraken.stop',
    ROUTING_KEY_START_KRAKEN: 'kraken.start',
    QUEUE_KRAKEN_HANDLER: 'MarketOrder_KrakenHandler'
  }
}
