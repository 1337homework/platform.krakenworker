module.exports = {
  proxyConnStr: process.env.HTTPS_PROXY || null,
  devLogger: process.env.DEV_LOGGER || true,
  wssPort: process.env.PORT || '8887',
  wssHost: process.env.WSS_HOST || 'localhost',
  krakenPollingInterval: process.env.KRAKEN_POLLING_INTERVAL || 5000,
  krakenAPIUrl: process.env.KRAKEN_API_URL || 'https://api.kraken.com/0/public/Ticker'
}
