const generalConfig = require('../../configuration/general')
const websockets = require('../../interfaces/websockets')
const Poller = require('../../utils/poller')
const ticker = require('../../utils/getTicker')

let poller = null

const stopPolling = () => {
  if (poller) {
    poller.stop()
    poller = null
  }
}

const startPolling = () => {
  if (poller) { return }

  const ws = websockets.getServer()

  poller = new Poller(generalConfig.krakenPollingInterval)

  poller.onPoll(async () => {
    let result
    try {
      result = await ticker()
    } catch (error) {
      result = error
    }

    ws.broadcast(result)
    poller.poll()
  })

  poller.poll()
}

module.exports = {
  startPolling,
  stopPolling
}
