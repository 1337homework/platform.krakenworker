FROM node:alpine

LABEL authors="Valdas Mazrimas <valdas.mazrimas@gmail.com>"
WORKDIR /srv/binanceworker

COPY package*.json ./
RUN npm install --only=production

COPY ./src ./src

EXPOSE 8887

CMD [ "npm", "start" ]